package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by calebdinsmore on 11/22/17.
 */
public class JoshKroll {
    static final String CHARACTER_NAME = "Mardak_";

    void applyGlow(Player player) {
        player.removePotionEffect(PotionEffectType.NIGHT_VISION);
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 2, true, false));
    }

    void checkForIronTool(Player player, BlockBreakEvent event) {
        if (player.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("diamond")
                || player.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("gold")) {
            event.setCancelled(true);
        }
    }

    void damagedEntity(Player player, EntityDamageByEntityEvent event) {
        ItemStack mainHand = player.getInventory().getItemInMainHand();
        String mainHandString = mainHand.getType().toString().toLowerCase();
        if (!mainHandString.contains("_pickaxe")
                || mainHandString.contains("gold")
                || mainHandString.contains("diamond")) {
            event.setDamage(0);
        }
    }

    void checkForHelmetEquipment(Player player) {
        if (player.getInventory().getHelmet() != null) {
            String helmetString = player.getInventory().getHelmet().getType().toString().toLowerCase();

            if (helmetString.contains("helmet") || helmetString.contains("leather")) {
                Inventory inventory = player.getInventory();
                ItemStack helmet = player.getInventory().getHelmet();
                inventory.addItem(helmet);
                player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
            }
        }
    }

    void checkForMoradinsWrath(Player player, Block blockBroken) {
        ItemStack pickaxe = player.getInventory().getItemInMainHand();

        if (pickaxe.getItemMeta().hasEnchant(Enchantment.DIG_SPEED)
                && pickaxe.getItemMeta().hasEnchant(Enchantment.MENDING)
                && pickaxe.getItemMeta().hasEnchant(Enchantment.PROTECTION_EXPLOSIONS)) {
            player.getWorld().createExplosion(blockBroken.getLocation(), 8);
            pickaxe.setDurability((short)(pickaxe.getDurability() + 7));
            player.updateInventory();
            if (pickaxe.getDurability() >= 250) {
                player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
            }
        }
    }

    void checkForRain(Player player) {
        if (!Common.solidBlockIsAbove(player)) {
            applyWaterNerf(player);
        }
    }

    void checkForWater(Player player) {
        Block playerBlock = player.getLocation().getBlock();
        if (playerBlock.getType() == Material.WATER || playerBlock.getType() == Material.STATIONARY_WATER) {
            applyWaterNerf(player);
        }
    }

    void itemCrafted(Player player, PrepareItemCraftEvent event) {
        if (event.getRecipe() == null) return;

        ItemStack itemCrafted = event.getRecipe().getResult();
        if (itemCrafted.getType() == Material.IRON_PICKAXE) {
            Random random = new Random();

            ItemMeta meta = itemCrafted.getItemMeta();

            meta.addEnchant(Enchantment.DIG_SPEED, 5, true);
            meta.addEnchant(Enchantment.DURABILITY, 3, true);
            meta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 3, true);
            meta.addEnchant(Enchantment.MENDING, 1, true);

            if (random.nextInt(250) == 100) {
                meta.setDisplayName("Moradin's Wrath");
                ArrayList<String> lore = new ArrayList<>();
                lore.add("Forged by the dwarven king, Mardak,");
                lore.add("this pickaxe has been blessed");
                lore.add("by the mighty Dwarven god, Moradin.");
                lore.add("His explosive wrath dwells within.");
                meta.setLore(lore);
                meta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 10, true);
            } else {
                meta.setDisplayName("Pickaxe of the Dwarven King");
                ArrayList<String> lore = new ArrayList<>();
                lore.add("This pick was forged");
                lore.add("by the hands of the");
                lore.add("mighty Dwarf king, Mardak.");
                meta.setLore(lore);
            }

            itemCrafted.setItemMeta(meta);

            event.getInventory().setResult(itemCrafted);
        } else if (itemCrafted.getType().toString().toLowerCase().contains("diamond")
                || itemCrafted.getType().toString().toLowerCase().contains("gold")) {
            itemCrafted.setType(Material.AIR);
            event.getInventory().setResult(itemCrafted);
        }
    }

    void processDamageEvent(Player player, EntityDamageEvent event) {
        switch(event.getCause()) {
            case FIRE:
            case FIRE_TICK:
            case LAVA:
                event.setCancelled(true);
                return;
        }

        if (player.hasPotionEffect(PotionEffectType.SLOW)
                && player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)
                && player.hasPotionEffect(PotionEffectType.WEAKNESS)) {
            event.setDamage(event.getDamage() * 2);
        }
    }

    private void applyWaterNerf(Player player) {
        ArrayList<PotionEffect> effects = new ArrayList<>();

        effects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, 12000, 1));
        effects.add(new PotionEffect(PotionEffectType.SLOW, 12000, 1));
        effects.add(new PotionEffect(PotionEffectType.WEAKNESS, 12000, 1));
        player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
        player.removePotionEffect(PotionEffectType.SLOW);
        player.removePotionEffect(PotionEffectType.WEAKNESS);
        player.addPotionEffects(effects);
    }
}
