package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;

import static org.bukkit.Bukkit.getServer;

/**
 * Author: calebdinsmore
 * Date: 1/9/17
 */
class RyanHouck {
    static final String CHARACTER_NAME = "Sparkler67";

    void damagedEntity(EntityDamageByEntityEvent event, Player player) {
        ItemStack mainHand = player.getInventory().getItemInMainHand();
        ItemStack offHand = player.getInventory().getItemInOffHand();

        // If item in both hands is an axe
        if (mainHand.getType().toString().toLowerCase().contains("_axe")
                && offHand.getType().toString().toLowerCase().contains("_axe")) {
            event.setDamage(event.getDamage() + calculateAdditionalOffHandDamage(offHand.getType()));
        } else {
            // Not holding two axes :(
            event.setDamage(0);
        }
    }

    void processPossibleAxeDamage(Player player, PlayerItemDamageEvent event) {
        ItemStack offHand = player.getInventory().getItemInOffHand();
        ItemStack eventItem = event.getItem();

        if (eventItem.getType().toString().toLowerCase().contains("_axe")
                && offHand.getType().toString().toLowerCase().contains("_axe")) {
            event.setDamage(event.getDamage() * 3);
        }
    }

    private int calculateAdditionalOffHandDamage(Material material) {
        switch (material) {
            case DIAMOND_AXE:
            case IRON_AXE:
            case STONE_AXE:
                return 9;
            case GOLD_AXE:
            case WOOD_AXE:
                return 7;
            default:
                return 7;
        }
    }
}
