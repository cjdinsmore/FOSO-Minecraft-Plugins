package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by calebdinsmore on 11/18/17.
 */
public class JoshRauh {
    static final String CHARACTER_NAME = "JPanda15";
    private int secondsSinceMovement;
    private Location lastLocation;

    JoshRauh() {
        secondsSinceMovement = 0;
    }

    void ateSugarCane(Player player) {
        player.playSound(player.getLocation(), Sound.ENTITY_GENERIC_EAT, SoundCategory.VOICE, 1.0F, 1.0F);
        player.setFoodLevel(player.getFoodLevel() + 1);
        player.setSaturation(Math.min(player.getFoodLevel(), player.getSaturation() + 10));
        player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_BURP, SoundCategory.VOICE, 1.0F, 1.0F);
    }

    void consumedItem(Player player, ItemStack item) {
        ArrayList<PotionEffect> effects = new ArrayList<>();
        switch(item.getType()) {
            case COOKED_BEEF:
            case COOKED_CHICKEN:
            case COOKED_FISH:
            case COOKED_MUTTON:
            case COOKED_RABBIT:
            case RAW_BEEF:
            case RAW_CHICKEN:
            case RAW_FISH:
            case PORK:
            case GRILLED_PORK:
                effects.add(new PotionEffect(PotionEffectType.POISON, 300, 3));
                effects.add(new PotionEffect(PotionEffectType.SLOW, 300, 1));
        }

        if (effects.size() > 0) player.addPotionEffects(effects);
    }

    void checkLocation(Player player, Location location, int secondInterval) {
        if (lastLocation == null) {
            setLastLocation(location);
            return;
        }

        if (Objects.equals(location.getBlock(), lastLocation.getBlock())) {
            secondsSinceMovement += secondInterval;
        } else {
            secondsSinceMovement = 0;
            setLastLocation(location);
        }

        if (secondsSinceMovement >= 9) {
            player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 70, 3));
        }
    }

    private void setLastLocation(Location location) {
        lastLocation = location;
    }
}
