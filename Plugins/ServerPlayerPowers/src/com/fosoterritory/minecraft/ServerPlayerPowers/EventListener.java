package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

import java.util.Objects;

import static org.bukkit.Bukkit.getServer;

/**
 * Author: calebdinsmore
 * Date: 7/27/16
 */
class EventListener implements Listener {
    private Rachael mRachael = new Rachael();
    private RyanHouck mRyanHouck = new RyanHouck();
    private Skaggs mSkaggs = new Skaggs();
    private JoshRauh mJoshRauh = new JoshRauh();
    private JoshKroll mJoshKroll = new JoshKroll();
    private Plugin mPlugin;

    EventListener(Plugin plugin) {
        mPlugin = plugin;
    }

    @EventHandler
    public void onItemCraftPrepare(PrepareItemCraftEvent event) {
        if (event == null) return;

        if (event.getViewers().size() == 0) return;

        Player player = (Player) event.getViewers().get(0);

        if (!Common.playerInNormalWorlds(player)) return;

        if (Objects.equals(player.getName(), JoshKroll.CHARACTER_NAME)) {
            mJoshKroll.itemCrafted(player, event);

        }

    }

    @EventHandler
    public void onPlayerRightClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (!Common.playerInNormalWorlds(player)) return;

        if (Objects.equals(player.getName(), Rachael.CHARACTER_NAME)
                && event.getAction() == Action.RIGHT_CLICK_BLOCK
                && event.getClickedBlock().getType() == Material.CROPS) {
            mRachael.rachaelRightClickTriggered(event.getClickedBlock());
        } else if (Objects.equals(player.getName(), JoshRauh.CHARACTER_NAME)
                && (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)
                && player.getInventory().getItemInMainHand().getType() == Material.SUGAR_CANE) {
            mJoshRauh.ateSugarCane(player);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Entity entity = event.getDamager();

        if (entity instanceof Player) {

            if (!Common.playerInNormalWorlds((Player)entity)) return;

            if (Objects.equals(((Player)entity).getPlayer().getName(), RyanHouck.CHARACTER_NAME)) {
                mRyanHouck.damagedEntity(event, (Player) entity);
            } else if (Objects.equals(((Player)entity).getPlayer().getName(), JoshKroll.CHARACTER_NAME)) {
                mJoshKroll.damagedEntity((Player) entity, event);
            }
        }
    }

    @EventHandler
    public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();

        if (!Common.playerInNormalWorlds(player)) return;

        if (Objects.equals(player.getName(), JoshRauh.CHARACTER_NAME)) {
            mJoshRauh.consumedItem(player, event.getItem());
        }

    }

    @EventHandler
    public void onPlayerItemDamage(PlayerItemDamageEvent event) {
        Player player = event.getPlayer();

        if (!Common.playerInNormalWorlds(player)) return;

        if (Objects.equals(player.getName(), RyanHouck.CHARACTER_NAME)) {
            mRyanHouck.processPossibleAxeDamage(player, event);
        }
    }

    @EventHandler
    public void onPlayerTakeDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        if (entity instanceof Player) {

            Player player = (Player)entity;

            if (!Common.playerInNormalWorlds((Player)entity)) return;

            if (Objects.equals(((Player)entity).getPlayer().getName(), Skaggs.CHARACTER_NAME)) {
                mSkaggs.tookDamage(event, (Player)entity);
            } else if (Objects.equals(player.getName(), JoshKroll.CHARACTER_NAME)) {
                mJoshKroll.processDamageEvent(player, event);
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!Common.playerInNormalWorlds(player)) return;

        if (Objects.equals(player.getName(), JoshKroll.CHARACTER_NAME)) {
            mJoshKroll.applyGlow(player);
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        if (!Common.playerInNormalWorlds(player)) return;

        if (Objects.equals(player.getName(), JoshKroll.CHARACTER_NAME)) {
            new BukkitRunnable(){
                @Override
                public void run(){
                    mJoshKroll.applyGlow(player);
                }
            }.runTaskLater(mPlugin, 3L);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();

        if (!Common.playerInNormalWorlds(player)) return;

        if (player.getInventory().getItemInMainHand() != null
                && player.getInventory().getItemInMainHand().getItemMeta() != null
                && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName() != null)

        if (Objects.equals(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName(), "Moradin's Wrath")) {
            mJoshKroll.checkForMoradinsWrath(player, event.getBlock());
        }

        if (Objects.equals(player.getName(), JoshKroll.CHARACTER_NAME)) {
            mJoshKroll.checkForIronTool(player, event);
        }
    }

    @EventHandler
    public void onDamageByBlock(EntityDamageByBlockEvent event) {
        if (event.getDamager() == null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemMend (PrepareAnvilEvent event) {
        if (event.getViewers().size() > 0) {
            Player player = (Player) event.getViewers().get(0);

            ItemStack item = event.getResult();
            if (item == null || item.getItemMeta() == null) return;
            String itemName = item.getItemMeta().getDisplayName();

            if (Objects.equals(itemName, "Moradin's Wrath")
                    || Objects.equals(itemName, "Pickaxe of Supreme Wreckage")) {
                player.sendMessage("No mortal can repair such a tool.");
                event.setResult(new ItemStack(Material.AIR));
            }
        }
        
    }
}
