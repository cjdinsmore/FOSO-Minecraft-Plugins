package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.material.Crops;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Objects;

/**
 * Author: calebdinsmore
 * Date: 7/27/16
 */
class Rachael {
    static final String CHARACTER_NAME = "reremae";

    void rachaelRightClickTriggered(Block targetedBlock) {
        applyBoneMealEffect(targetedBlock);
    }

    void recurringRachaelEvents(Player player) {
        if (!Common.playerInNormalWorlds(player)) return;

        Hashtable<Material, ArrayList<Block>> blockTable = Common.getNearbyBlocks(player.getLocation(), 10); // Get blocks within 20 radius

        attemptFurnaceNerf(blockTable, player);
        attemptNatureBuff(blockTable, player);
    }

    private boolean attemptFurnaceNerf(Hashtable<Material, ArrayList<Block>> blockTable, Player player) {
        if (blockTable.get(Material.FURNACE) != null) {
            int nearbyFurnaces = blockTable.get(Material.FURNACE).size();

            PotionEffect harm = new PotionEffect(PotionEffectType.WITHER, 65, nearbyFurnaces);
            PotionEffect confusion = new PotionEffect(PotionEffectType.CONFUSION, 65, nearbyFurnaces);

            player.addPotionEffect(harm, true);
            player.addPotionEffect(confusion, true);
            return true;
        }
        return false;
    }

    private void attemptNatureBuff(Hashtable<Material, ArrayList<Block>> blockTable, Player player) {
        int count = 0;

        ArrayList<Block> blockList = blockTable.get(Material.WOOD);
        if (blockList != null)
            count += blockList.size();

        blockList = blockTable.get(Material.LEAVES);
        if (blockList != null)
            count += blockList.size();

        blockList = blockTable.get(Material.GRASS);
        if (blockList != null)
            count += blockList.size();

        blockList = blockTable.get(Material.CROPS);
        if (blockList != null)
            count += blockList.size();

        if (count >= 200) {
            PotionEffect damageResist = new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 65, count/250);
            PotionEffect regen = new PotionEffect(PotionEffectType.REGENERATION, 65, count/10);
            PotionEffect glow = new PotionEffect(PotionEffectType.GLOWING, 65, count/10);

            player.addPotionEffect(damageResist, true);
            player.addPotionEffect(regen, true);
            player.addPotionEffect(glow, true);
        }

    }

    private void applyBoneMealEffect(Block targetedBlock) {
        Crops crops = new Crops(CropState.RIPE);
        BlockState blockState = targetedBlock.getState();
        blockState.setData(crops);
        blockState.update();
    }
}
