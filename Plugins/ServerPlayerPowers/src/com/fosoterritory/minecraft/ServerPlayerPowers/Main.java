package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

/**
 * Author: calebdinsmore
 * Date: 7/27/16
 */
public class Main extends JavaPlugin {
    private Rachael mRachael = new Rachael();
    private JoshRauh mJoshRauh = new JoshRauh();
    private JoshKroll mJoshKroll = new JoshKroll();

    @Override
    public void onEnable() {
        Server server = getServer();

        server.getPluginManager().registerEvents(new EventListener(this), this);

        server.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            loopThroughPlayers();
        }, 0/* delay before first execution */, 60 /* delay between executions */);
    }

    @Override
    public void onDisable() {
    }

    private void loopThroughPlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!Common.playerInNormalWorlds(player)) return;
            if (Objects.equals(player.getName(), Rachael.CHARACTER_NAME)) {
                mRachael.recurringRachaelEvents(player);
            } else if (Objects.equals(player.getName(), JoshRauh.CHARACTER_NAME)) {
                mJoshRauh.checkLocation(player, player.getLocation(), 3);
            } else if (Objects.equals(player.getName(), JoshKroll.CHARACTER_NAME)) {
                if (player.getWorld().hasStorm()) {
                    mJoshKroll.checkForRain(player);
                }
                mJoshKroll.checkForWater(player);
                mJoshKroll.checkForHelmetEquipment(player);
            }
        }
    }
}
