package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import static org.bukkit.Bukkit.getServer;

/**
 * Author: calebdinsmore
 * Date: 1/10/17
 */
class Skaggs {
    static final String CHARACTER_NAME = "Shiverburn98";

    void tookDamage(EntityDamageEvent event, Player player) {
        double healthBeforeDamage = player.getHealth();
        double healthAfterDamage = healthBeforeDamage - event.getFinalDamage();
        double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();

        if (healthBeforeDamage / maxHealth > .5
                && healthAfterDamage / maxHealth <= .5) {
            getServer().dispatchCommand(player, "home");
            player.sendMessage("You wimped out!");
        }
    }
}
