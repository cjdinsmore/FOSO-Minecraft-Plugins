package com.fosoterritory.minecraft.ServerPlayerPowers;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Author: calebdinsmore
 * Date: 7/27/16
 */
class Common {
    static boolean arrayContainsObject(Object[] array, Object object) {
        for (Object arrObj : array) {
            if (arrObj.equals(object)) {
                return true;
            }
        }
        return false;
    }
    static Block getBlockUnderPlayer(Player player) {
        return player.getLocation().getBlock().getRelative(BlockFace.DOWN);
    }

    static Hashtable<Material, ArrayList<Block>> getNearbyBlocks(Location location, int radius) {
        Hashtable<Material, ArrayList<Block>> blockTable = new Hashtable<>();
        for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
            for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
                for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
                    Block block = location.getWorld().getBlockAt(x, y, z);
                    if (blockTable.containsKey(block.getType())) {
                        blockTable.get(block.getType()).add(block);
                    } else {
                        ArrayList<Block> blockList = new ArrayList<>();
                        blockList.add(block);

                        blockTable.put(block.getType(), blockList);
                    }
                }
            }
        }
        return blockTable;
    }

    static boolean playerInNormalWorlds(Player player) {
        switch(player.getLocation().getWorld().getName()) {
            case "world":
            case "world_nether":
            case "world_the_end":
                return true;
            default:
                return false;
        }
    }

    static boolean solidBlockIsAbove(Player player) {
        Location location = player.getLocation();
        for (int y = player.getLocation().getBlockY() + 1; y < 250; y++) {
            Block block = player.getWorld().getBlockAt(location.getBlockX(), y, location.getBlockZ());
            if (block.getType() != Material.AIR) return true;
        }
        return false;
    }
}
